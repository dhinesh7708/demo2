package com.example.demo.service;

import org.springframework.stereotype.Service;

@Service
public class ServiceFile {
	public String print() {
		return "Hello Java...!";
	}
	public String getdata()
	{
		return "Hello Spring Boot...!";
	}
}
